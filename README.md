# CNC_Interface

This is an interface for a grbl CNC interpreter [Quick Start Guide](https://bitbucket.org/testtechsolutions/cnc_interface).


## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://bitbucket.org/testtechsolutions/cnc_interface
# Go into the repository
cd CNC_Interface
# Install dependencies
npm install
# Run the app
npm run postinstall
npm start
```


## License

[CC0 1.0 (Public Domain)](LICENSE.md)
