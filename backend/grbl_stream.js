const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const lineReader = require('line-reader');
const EventEmitter = require('events');



const GRBL_ERROR = {
  1:  `G-code words consist of a letter and a value. Letter was not found.`,
  2: 	`Numeric value format is not valid or missing an expected value.`,
  3: 	`Grbl '$' system command was not recognized or supported.`,
  4: 	`Negative value received for an expected positive value.`,
  5: 	`Homing cycle is not enabled via settings.`,
  6: 	`Minimum step pulse time must be greater than 3usec`,
  7: 	`EEPROM read failed. Reset and restored to default values.`,
  8: 	`Grbl '$' command cannot be used unless Grbl is IDLE. Ensures smooth operation during a job.`,
  9: 	`G-code locked out during alarm or jog state`,
  10: 	`Soft limits cannot be enabled without homing also enabled.`,
  11: 	`Max characters per line exceeded. Line was not processed and executed.`,
  12: 	`(Compile Option) Grbl '$' setting value exceeds the maximum step rate supported.`,
  13: 	`Safety door detected as opened and door state initiated.`,
  14: 	`(Grbl-Mega Only) Build info or startup line exceeded EEPROM line length limit.`,
  15: 	`Jog target exceeds machine travel. Command ignored.`,
  16: 	`Jog command with no '=' or contains prohibited g-code.`,
  17: 	`Laser mode requires PWM output.`,
  20: 	`Unsupported or invalid g-code command found in block.`,
  21: 	`More than one g-code command from same modal group found in block.`,
  22: 	`Feed rate has not yet been set or is undefined.`,
  23: 	`G-code command in block requires an integer value.`,
  24: 	`Two G-code commands that both require the use of the XYZ axis words were detected in the block.`,
  25: 	`A G-code word was repeated in the block.`,
  26: 	`A G-code command implicitly or explicitly requires XYZ axis words in the block, but none were detected.`,
  27: 	`N line number value is not within the valid range of 1 - 9,999,999.`,
  28: 	`A G-code command was sent, but is missing some required P or L value words in the line.`,
  29: 	`Grbl supports six work coordinate systems G54-G59. G59.1, G59.2, and G59.3 are not supported.`,
  30: 	`The G53 G-code command requires either a G0 seek or G1 feed motion mode to be active. A different motion was active.`,
  31: 	`There are unused axis words in the block and G80 motion mode cancel is active.`,
  32: 	`A G2 or G3 arc was commanded but there are no XYZ axis words in the selected plane to trace the arc.`,
  33: 	`The motion command has an invalid target. G2, G3, and G38.2 generates this error, if the arc is impossible to generate or if the probe target is the current position.`,
  34: 	`A G2 or G3 arc, traced with the radius definition, had a mathematical error when computing the arc geometry. Try either breaking up the arc into semi-circles or quadrants, or redefine them with the arc offset definition.`,
  35: 	`A G2 or G3 arc, traced with the offset definition, is missing the IJK offset word in the selected plane to trace the arc.`,
  36: 	`There are unused, leftover G-code words that aren't used by any command in the block.`,
  37: 	`The G43.1 dynamic tool length offset command cannot apply an offset to an axis other than its configured axis. The Grbl default axis is the Z-axis.`,
  38: 	`Tool number greater than max supported value.`
}
const GRBL_ALARM = {
  1: 	`Hard limit triggered. Machine position is likely lost due to sudden and immediate halt. Re-homing is highly recommended.`,
  2: 	`G-code motion target exceeds machine travel. Machine position safely retained. Alarm may be unlocked.`,
  3: `Reset while in motion. Grbl cannot guarantee position. Lost steps are likely. Re-homing is highly recommended.`,
  4: 	`Probe fail. The probe is not in the expected initial state before starting probe cycle, where G38.2 and G38.3 is not triggered and G38.4 and G38.5 is triggered.`,
  5: 	`Probe fail. Probe did not contact the workpiece within the programmed travel for G38.2 and G38.4.`,
  6: 	`Homing fail. Reset during active homing cycle.`,
  7: 	`Homing fail. Safety door was opened during active homing cycle.`,
  8: 	`Homing fail. Cycle failed to clear limit switch when pulling off. Try increasing pull-off setting or check wiring.`,
  9: 	`Homing fail. Could not find limit switch within search distance. Defined as 1.5 * max_travel on search and 5 * pulloff on locate phases.`,
}
let getSettings = {}

class Grbl extends EventEmitter
{
  constructor(comPort)
  {
    super();
    this.pollInterval = null;
    this.port = new SerialPort(comPort, { baudRate: 115200 })
    this.port.on('error',(err)=>{
      this.emit('err', err)
    })
    this.timeoutExpired = ()=>{
      this.emit('err', 'GRBL communications timeout, closing connection')
      this.cleanup();
    }
    this.alarms = []
    this.timeout = null;
    this.resetTimeout = (timeout)=>{
      if(this.timeout){
        clearTimeout(this.timeout)
      }
      this.timeout = setTimeout(this.timeoutExpired, timeout);
    }
    this.parser = new Readline()
    this.port.pipe(this.parser);
    const GRBL_RX_SZ = 128
    this.initProgram = ()=>{
      this.gCodeProgram = {
        active: false,
        activeLine: 0,
        nextLineToSend: 0,
        lines: [],
        linesSent: [],//lines in buffer
        charsAvail: GRBL_RX_SZ,//room in grbl's buffer
      }
    }
    this.initProgram();
    this.makeDivContent = ()=>{
      const DONE_LINES = 4;//number of lines to show that are completed
      const IN_BUFFER = this.gCodeProgram.linesSent.length -1;//number to show highlighed below active
      const TOTAL_LINES = 20; //total lines in the div at one time
      let text ='';
      let line = this.gCodeProgram.nextLineToSend - this.gCodeProgram.linesSent.length;//active line
      let startLine = Math.max(0, line-DONE_LINES)
      for(let l = startLine; l < Math.min(startLine + TOTAL_LINES, this.gCodeProgram.lines.length); l++){
        let span = ''
        let content = `Line ${l+1} of ${this.gCodeProgram.lines.length}:  ${this.gCodeProgram.lines[l]}`
        switch(true){
          case(l<line):
            span+=`<p class='gcode-done'><span>${content}</span></p>`;
            break;
          case(l==line):
            span+=`<p class='gcode-active'><span>${content}</span></p>`;
            break;
          case((l>line) && (l<=(line+IN_BUFFER))):
            span+=`<p class='gcode-buffer'><span>${content}</span></p>`;
            break
          case(l>(line+IN_BUFFER)):
            span+=`<p class='gcode-waiting'><span>${content}</span></p>`;
            break;
          }
        text += `${span}`
      }
      this.emit('gcode-program-update', text);
    }


    this.machineInit = ()=>
      {this.machine =
        {
          connectionStatus: false,
          jogDistance: {x: 1, y:1, z: 0.2},
          jogFeedrate: {x: 500, y:500, z: 500},
          position: {x:null, y:null, z:null},
          Wpos: {x:null, y:null, z:null},
          overRides: {feed: null, rapid: null, spindle: null},
          coolant: null, //0 = off, 1 = Flood, 2=Mist, 3=both??
          spindle: null, //0 = off, 1 = Fwd, 2=Rev
          state: null,
          pin: {x: null, y: null, z:null, door: null, hold: null, softReset: null, cycleStart: null},
          speed: {feed: null, spindle: null},
          buffer: {blocksIn: null, bytesAvailible: null},
          workCoordinateOffset: {x:null, y:null, z:null},
        }
      }

    this.machineInit();

    this.update = (line)=>
    {
      //pin updates only show up if true, init to false and if they are in param they will set to true
      this.machine.connectionStatus = true;
      this.machine.pin.x = false;
      this.machine.pin.y = false;
      this.machine.pin.z = false;
      this.machine.pin.door = false;
      this.machine.pin.hold = false;
      this.machine.pin.softReset = false;
      this.machine.pin.cycleStart = false;
      line = line.replace(RegExp(`[<]`, 'g'), '')
      line = line.replace(RegExp(`[>]`, 'g'), '')
      line = line.replace(RegExp(`\r`, 'g'), '')
      line = line.replace(RegExp(`\n`, 'g'), '')
      let params
      let calcWCO = false
      if (line.includes("|")){
        //For GRBL version 1.1 a '|' separator is used not a ','
        params = line.split('|')
      }else{
        if (line.includes("MPos") && line.includes("WPos")){
          calcWCO = true
        }
        //This is needed to interface with GRBL versions older than 1.1
        line = line.replace(',MPos:','|MPos:')
        line = line.replace(',WPos:','|WPos:')
        params = line.split('|')
        
      }
      for(let idx in params)
      {
        let val= params[idx]
        let vals;
        switch(true){
          case(idx==0):
            this.machine.state = val;
            break;
          case(val.startsWith('MPos')):
            //Machine Position
            vals = val.replace(RegExp(`[MPos:]`, 'g'), '').split(',');
            this.machine.position.x = Number(vals[0]);
            this.machine.position.y = Number(vals[1]);
            this.machine.position.z = Number(vals[2]);
            break;
          case(val.startsWith('WPos')):
            //Work Position
            vals = val.replace(RegExp(`[WPos:]`, 'g'), '').split(',');
            this.machine.Wpos.x = Number(vals[0]);
            this.machine.Wpos.y = Number(vals[1]);
            this.machine.Wpos.z = Number(vals[2]);
            break;
          case(val.startsWith('Bf')):
            vals = val.replace(RegExp(`[Bf:]`, 'g'), '').split(',');
            this.machine.buffer.blocksIn = Number(vals[0]);
            this.machine.buffer.bytesAvailible = Number(vals[1]);
            break;
          case(val.startsWith('FS')):
            vals = val.replace(RegExp(`[FS:]`, 'g'), '').split(',');
            this.machine.speed.feed = Number(vals[0]);
            this.machine.speed.spindle = Number(vals[1]);
            break;
          case(val.startsWith('Ov')):
            vals = val.replace(RegExp(`[Ov:]`, 'g'), '').split(',');
            this.machine.overRides.feed = Number(vals[0]);
            this.machine.overRides.rapid = Number(vals[1]);
            this.machine.overRides.spindle = Number(vals[2]);
            //init these since this line would contain status if they are on
            this.machine.spindle = 0;
            this.machine.coolant = 0;
            break;
          case(val.startsWith('WCO')):
            //Work Coordinate Offset
            //WCO = MPos - WPos
            vals = val.replace(RegExp(`[WCO:]`, 'g'), '').split(',');
            this.machine.workCoordinateOffset.x = Number(vals[0]);
            this.machine.workCoordinateOffset.y = Number(vals[1]);
            this.machine.workCoordinateOffset.z = Number(vals[2]);
            break;
          case(val.startsWith('Pn')):
            vals = val.replace(RegExp(`[Pn:]`, 'g'), '');
            this.machine.pin.x = (vals.indexOf('X') >= 0);
            this.machine.pin.y = (vals.indexOf('Y') >= 0);
            this.machine.pin.z = (vals.indexOf('Z') >= 0);
            this.machine.pin.doorHold = (vals.indexOf('D') >= 0);
            this.machine.pin.hold = (vals.indexOf('H') >= 0);
            this.machine.pin.softReset = (vals.indexOf('R') >= 0);
            this.machine.pin.cycleStart = (vals.indexOf('S') >= 0);
            break;
          case(val.startsWith('A')):
            vals = val.replace(RegExp(`[A:]`, 'g'), '');
            this.machine.spindle = (2 * (vals.indexOf('C') >= 0)) + (vals.indexOf('S') >= 0);
            this.machine.coolant = (2 * (vals.indexOf('M') >= 0)) + (vals.indexOf('F') >= 0);
            break;
          default:
            this.emit('err', `unhandled parameter in grbl parse: ${val}`)
            //throw(Error(`unhandled parameter in grbl parse: ${val}`))
        }
      }
      if (calcWCO){
        this.machine.workCoordinateOffset.x = this.machine.position.x-this.machine.Wpos.x
        this.machine.workCoordinateOffset.y = this.machine.position.y-this.machine.Wpos.y
        this.machine.workCoordinateOffset.z = this.machine.position.z-this.machine.Wpos.z
        calcWCO = false
      }
    }

    this.sendProgram = async (code)=>{
      if(this.gCodeProgram.active){
        return; //dont cross the streams!!!
      }
      if(code.endsWith('\n')){
        code = code.slice(0, -1);
      }
      this.gCodeProgram.lines = code.split('\n');
      this.gCodeProgram.active = true;
      this.writeGcode();
    }
    this.reset = ()=>{
      if(this.machine.state =="Alarm"){
        this.sendLine('$X\n');//clear alarm
        return;
      }
      this.sendLine(String.fromCharCode(0x18)+'\n')
      this.initProgram();
      this.emit('gcode-program-finish', {});
    }
    this.sendLine = (line)=>{
      console.log('Sending: ' +line); 
      this.port.write(line);
    }

    this.home = ()=>{
      this.sendLine('$H\n')
      this.machine.state = 'Homing';
      this.emit('poll', JSON.stringify(this.machine))
      this.resetTimeout(120000);
      //overide limit switches in PLC
    }

    this.writeGcode = ()=>
    {
      if(this.gCodeProgram.nextLineToSend >= this.gCodeProgram.lines.length){
        //console.log("All sent");
        this.emit('gcode-program-finish', {});
        this.initProgram();
      }
      if(!this.gCodeProgram.active){
        return;
      }
      this.calcCharsAvail();
      while(this.gCodeProgram.active &&
        this.gCodeProgram.nextLineToSend < this.gCodeProgram.lines.length &&
        this.gCodeProgram.lines[this.gCodeProgram.nextLineToSend].length < this.gCodeProgram.charsAvail ){
          this.sendProgramLine(this.gCodeProgram.lines[this.gCodeProgram.nextLineToSend]);
          this.calcCharsAvail();
        }
    }

    this.calcCharsAvail = ()=>{
      this.gCodeProgram.charsAvail = GRBL_RX_SZ;
      for(let l in this.gCodeProgram.linesSent){
        this.gCodeProgram.charsAvail -= this.gCodeProgram.linesSent[l].length
      }
    }

    this.sendProgramLine = (line)=>{
      line = line+'\n';
      this.gCodeProgram.linesSent.push(line)
      this.sendLine(line);
      this.gCodeProgram.nextLineToSend +=1;
      this.makeDivContent();
    }

    this.parser.on('data', (line)=>{
      //console.log('line',line)
      if(this.machine.state == 'Homing'){
        console.log('Done homing, hommie')
      }

      this.resetTimeout(5000);
      switch(true){
        case(line.startsWith('<')):
          this.update(line);
          //console.log('lines',line)
          //console.log('machine',this.machine)
          getSettings = {}
          break;
        case(line.startsWith('ok')):
          if(this.gCodeProgram.active){
            this.gCodeProgram.activeLine += 1;
            let l = this.gCodeProgram.linesSent.shift()
            this.writeGcode();
          }
          getSettings = {}
          break;
        case(line.startsWith('ALARM:')):
          let alm_idx = Number(line.replace(RegExp(`[^0-9].`, 'g'), ''));
          this.emit('err', `${GRBL_ALARM[alm_idx]}`);
          getSettings = {}
          break;
        case(line.startsWith('error:')):
          let err_idx = Number(line.replace(RegExp(`[^0-9].`, 'g'), ''));
          this.emit('err', `${GRBL_ERROR[err_idx]}`);
          getSettings = {}
          break;
        case(line.startsWith('$')):

          var patt = /\.*\d+\.*\d*/g; //match numeric digits of quantity 1 or more
          var result = line.match(patt);
          if(result.length!=2 && result.length!=3){
            //console.log(`grbl command issue`)
            break; // something bad :(
          }
          this.emit('gcode-settings-received', {"cmd":Number(result[0]), "val": Number(result[1])});
          break;
        default:
          getSettings = {}
        }        
        this.emit('poll', JSON.stringify(this.machine))
        
    });

    this.poll = ()=>
    {
      if(!this.port.isOpen){
        this.machineInit(); 
      }else{
        this.port.write('?');
      }//ask for update
    }
    this.run_once = ()=>
    {
      if(!this.port.isOpen){
        this.machineInit(); 
      }else{
        this.port.write(`$$\n`);
      }//ask for update
    }

    this.connect = ()=>
    {
      this.port.write('?');
      this.pollInterval = setInterval(this.poll, 250);
      this.once = setTimeout(this.run_once, 1000);
      this.resetTimeout(5000);
    }
    
    this.cleanup = ()=>{
      if(this.pollInterval){
        clearInterval(this.pollInterval);
        this.pollInterval = null;
      }
      if(this.port.isOpen){
        this.port.close()
      }
      this.machineInit();
      this.emit('poll', JSON.stringify(this.machine))//send update
    }
    
  }
}
module.exports = {
  Grbl}