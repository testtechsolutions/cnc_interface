const EventEmitter = require('events');
const Modbus = require('jsmodbus');
const SerialPort = require('serialport')
class Plc extends EventEmitter
{
  constructor(comPort)
  {
    super();
    this.tags =
    {
      "LCK_OUT_OK": {Address: 0},
      "SPDL_RUN_STATE": {Address: 1},
      "SPDL_DIR_STATE": {Address: 2},
      "LCK_OUT_RST": {Address: 16},
      "LS_OVR_RDE": {Address: 17},
      "DRV_DS_EBL": {Address: 18},
      "SPDL_RUN_CMD": {Address: 19},
      "SPDL_STOP_CMD": {Address: 20},
      "SPDL_FWD_CMD": {Address: 21},
      "SPDL_REV_CMD": {Address: 22},
      "TC_IN_CMD": {Address: 23},
      "TC_OUT_CMD": {Address: 24},
      "BRK_ON_CMD": {Address: 25},
      "BRK_OFF_CMD": {Address: 26},
      "E_STP_OK": {Address: 96},
      "LS_X_PLS": {Address: 101},
      "LS_X_MNS": {Address: 102},
      "LS_Y_PLS": {Address: 103},
      "LS_Y_MNS": {Address: 104},
      "LS_Z_PLS": {Address: 105},
      "LS_Z_MNS": {Address: 106},
      "SPDL_DRV_OK": {Address: 107},
      "DRV_ENBL": {Address: 112},
      "SPDL_RUN": {Address: 114},
      "SPDL_REV": {Address: 115},
      "COOL_ON":  {Address: 116},
      "OILER_ON": {Address: 117},
      "BRK_ON":   {Address: 122},
      "TOOL_IN":  {Address: 125},
      "TOOL_OUT": {Address: 126}
    }
    this.socket = null;
    this.connection = null;
    this.pollInterval = null;
    this.timeout = null;
    this.writes = [] //
    this.cleanup = ()=>{
      if(this.pollInterval){
        clearInterval(this.pollInterval);
        this.pollInterval = null;
      }
      if(this.socket){
        this.socket.close();
        this.socket = null;
      }
    }

    this.timeoutExpired = ()=>{
      this.emit('err', 'Communications timeout, closing connection')
      this.cleanup();
    }

    
    this.resetTimeout = ()=>{
      if(this.timeout){
        clearTimeout(this.timeout);
      }
      this.timeout = setTimeout(this.timeoutExpired, 2000);
    }
    this.pollInterval = null;
    this.socket = new SerialPort(comPort, {baudRate: 19200})
    this.socket.on('open', ()=> {
      this.connect();
    })

    this.socket.on('error', (err)=>{
      this.emit('err', err)  
    })
    this.connection = new Modbus.client.RTU(this.socket, 1);

    this.poll = async ()=>{
      try{
        if(this.writes.length){
          let write = this.writes.shift()
          let resp = await this.connection.writeSingleCoil(this.tags[write.tag].Address, write.val);
          return;
        }
        let resp = await this.connection.readCoils(0, 128);
        let vals = resp.response.body.valuesAsArray
        let tags = {}
        for(let t in this.tags){
          let tag = this.tags[t]
          tags[t] = vals[tag.Address]
        }
        this.emit('poll', JSON.stringify(tags))
        this.resetTimeout();
      }
      catch(err){
        this.emit('err', err);
        //this.cleanup();
      }
    }
    this.writeTag = (tag, val)=>{
      this.writes.push({tag,val})
    }


    this.connect = ()=>{
      this.cleanup();
      this.pollInterval = setInterval(this.poll, 250);
      this.resetTimeout();
    }
  }
}


module.exports = {
  Plc}