// Modules to control application life and create native browser window
const {app, BrowserWindow, ipcMain, dialog} = require('electron')
const os = require('os');
const fs = require('fs')
const path = require('path')
const {Plc} = require('./backend/plc')
const {Grbl} = require('./backend/grbl_stream')
const SerialPort = require('serialport')
let mainWindow = null; // hold the global window ref
let popupWindow = null; // hold the global window ref
let grbl = null;
let plc = null;



ipcMain.on('openGcodeFile', async (event, _) => {
  try{
    let filters = []
    let op = {
      "title" : "Select a File(s) to Open", 
      "buttonLabel" : "Select",
      "filters" : filters,
      // ,'multiSelections' Allows files to be selected, Allows multiple files to be selected
      properties: ['openFile']
     }
     let status = await dialog.showOpenDialog(op);
     if(!status.canceled){
       let cb = (err, data)=>{
         if(err){throw err}
         let s = data.toString('utf8')
         sendToWindow('gcode-load', JSON.stringify(s));
        }
        fs.readFile(status.filePaths[0], cb)
     }
     
  }
  catch(rejection){
    PostAlarm('Alm_message',rejection)

  }
}); 

ipcMain.on('getGRBLSettings', async (event, _) => {
  getGRBLSet()
}); 

ipcMain.on('openGRBLSettingsFile', async (event, _) => {
  try{
    let filters = [{ name: 'Settings Files', extensions: ['grbl'] },
    { name: 'All Files', extensions: ['*'] }]
    let op = {
      "title" : "Select a File(s) to Open", 
      "buttonLabel" : "Select",
      "filters" : filters,
      // ,'multiSelections' Allows files to be selected, Allows multiple files to be selected
      properties: ['openFile']
     }
     let status = await dialog.showOpenDialog(op);
     if(!status.canceled){
       let cb = (err, data)=>{
         if(err){throw err}
         let s = data.toString('utf8')
        let arr = {}
        var words = s.split("\r")
        for(let itm in words){
          var [temp,temp2] = words[itm].split(':')
          temp = temp.replace(/\n/g, '')
          arr[temp]=temp2
        }
        sendToWindow('GRBLSettings-load', JSON.stringify(arr));
        }
        fs.readFile(status.filePaths[0], cb)
     }
     
  }
  catch(rejection){
    PostAlarm('Alm_message',rejection)

  }
}); 

ipcMain.on('saveGcodeFile', async (event, payload) => {
  try{
    let filters = [{ name: 'Machine Code Files', extensions: ['nc'] },
    { name: 'All Files', extensions: ['*'] }]
    let op = {
      "title" : "Save File", 
      //"buttonLabel" : "Select",
      "filters" : filters,
      // ,'multiSelections' Allows files to be selected, Allows multiple files to be selected
      properties: ['openFile']
     }
     let status = await dialog.showSaveDialog(op);
     if(!status.canceled){
        fs.writeFile(status.filePath, JSON.parse(payload), (err,_)=>{if(err)throw(err)})
     }
     
  }
  catch(rejection){
    PostAlarm('Alm_message',rejection)

  }
}); 

ipcMain.on('saveGRBLsettings', async (event, payload) => {
  try{
    let filters = [{ name: 'Settings Files', extensions: ['grbl'] },
    { name: 'All Files', extensions: ['*'] }]
    let op = {
      "title" : "Save File", 
      //"buttonLabel" : "Select",
      "filters" : filters,
      // ,'multiSelections' Allows files to be selected, Allows multiple files to be selected
      properties: ['openFile']
     }
     let status = await dialog.showSaveDialog(op);
     if(!status.canceled){
        test = JSON.parse(payload)
        var logger = fs.createWriteStream(status.filePath, {
          flags: 'w' // 'w' means write (old data will be overwritten)
        })
        for (var idx in test) {
          logger.write(idx.concat(test[idx])) // append string to your file
          logger.write("\r\n")
        }
        logger.end() // close string
     }
  }
  catch(rejection){
    PostAlarm('Alm_message',rejection)

  }
});



const sendToWindow = (signal, payload)=>{
  if (mainWindow){
    mainWindow.webContents.send(signal, payload);
  }
}

const PostAlarm = (signal, payload)=>{
  if (mainWindow){
    mainWindow.webContents.send(signal, payload);
  }
}

ipcMain.on('cmd', async (e, payload)=>{
  let cmd = JSON.parse(payload)
  switch(true){
    case(cmd.command == "sendGcode"):
      if(grbl){
        grbl.sendLine(cmd.params[0])
      }
      break;
    case(cmd.command == "sendGcode-line"):
      if(grbl){
        grbl.sendLine(cmd.params[0]+'\n')
      }
      break;
    case(cmd.command == "sendGcode-program"):
      if(grbl){
        try{
          await confirm('Run Program?');
          grbl.sendProgram(cmd.params[0]);
        }
        catch(err){
        }
      }
      break;
    case(cmd.command == "zeroAxis"):
      if(grbl){
        grbl.sendLine(`G10 P0 L20 ${cmd.params[0]}0\n`)
      }
      break;
    case(cmd.command == 'jog'):    
      if(grbl){
        switch(true){
           case(cmd.params.btn=="X-Jog"):
            grbl.sendLine(`$J=G21G91X-${grbl.machine.jogDistance.x} F${grbl.machine.jogFeedrate.x}\n`)
            break;
          case(cmd.params.btn=="X+Jog"):
            grbl.sendLine(`$J=G21G91X${grbl.machine.jogDistance.x} F${grbl.machine.jogFeedrate.x}\n`)
            break;
          case(cmd.params.btn=="Y-Jog"):
            grbl.sendLine(`$J=G21G91Y-${grbl.machine.jogDistance.y} F${grbl.machine.jogFeedrate.y}\n`)
            break;
          case(cmd.params.btn=="Y+Jog"):
            grbl.sendLine(`$J=G21G91Y${grbl.machine.jogDistance.y} F${grbl.machine.jogFeedrate.y}\n`)
            break;
          case(cmd.params.btn=="Z-Jog"):
            grbl.sendLine(`$J=G21G91Z-${grbl.machine.jogDistance.z} F${grbl.machine.jogFeedrate.z}\n`)
            break;
          case(cmd.params.btn=="Z+Jog"):
            grbl.sendLine(`$J=G21G91Z${grbl.machine.jogDistance.z} F${grbl.machine.jogFeedrate.z}\n`)
            break;
        }
      }
      break;
    case(cmd.command == 'singleSetting'):   
      if(grbl){
        try{
          if (cmd.params[1].length != 0){
            let msg = `Send ${cmd.params[2]} = ${cmd.params[1]}`
            await confirm(`${msg}`);
            /////////////////////////This is where you take the payload.cmd and payload.value to send command to controller
            grbl.sendLine(`$${cmd.params[0]}=${cmd.params[1]}\n`)
            getGRBLSet()
          }
          else{
            alert(`Value Error`);
            PostAlarm('Alm_message','Setting Value Error')
            
          }
        }
        catch(err){
          //reject is a cancel, do nothing
          if (err != 'Canceled'){
            PostAlarm('Alm_message',err)
          }
        }
          
      }else{
        alert(`Communication Failure to GRBL Controller`);
        PostAlarm('Alm_message', 'Communication Failure to GRBL Controller');
      }
      break;
    
    case(cmd.command == 'multiSetting'):  
      if(grbl){
        try{
          await confirm('Send All Settings');
          for (var idx in cmd.params) {
            grbl.sendLine(`$${cmd.params[idx].cmd}=${cmd.params[idx].val}\n`)
          }
          getGRBLSet()

        }
        catch(err){
          //reject is a cancel, do nothing
          alert('Error in Sending Settings');
          PostAlarm('Alm_message',err)
        }
      }else{
        alert(`Communication Failure to GRBL Controller`);
        PostAlarm('Alm_message', 'Communication Failure to GRBL Controller');
      }
      break;


    default:
      PostAlarm('Alm_message','Unknown command sent from page')

  }
})
//plcWrite
ipcMain.on('plcWrite', (e, payload)=>{
  let params = JSON.parse(payload)
  if(plc){
    plc.writeTag(params.tag, params.val);
  }
})

const getPortsList = () => {
    SerialPort.list().then(
     ports => {
       if (ports.length){
        sendToWindow('com-ports', ports);
       }else{
         alert(`No communication ports found try reconnecting controllers and refreshing`)
       }
      
     },
     err => {
       PostAlarm('Alm_message','Error listing ports')
       PostAlarm('Alm_message',err)
     }
    )
};

ipcMain.on('temp-code', async (e, payload)=>{
  try{
    await confirm(`Generate template code and overwrite data on Program tab`);
  }
  catch(err){
    //reject is a cancel, do nothing
    if (err != 'Canceled'){
      PostAlarm('Alm_message',err)
    }
    
  }
})

ipcMain.on('comm-refresh', (e, payload)=>{getPortsList();})
/* ipcMain.on('debug-data', (e, payload)=>{
  console.log(JSON.parse(payload));
}) */
ipcMain.on('app-exit', async (e, payload)=>{
    try{
      await confirm(`Exit Testtech-CNC?`);
      params = {}
      params['controller']='all'
      disconnectComms(params)
      app.quit();
    }
    catch(err){
      //reject is a cancel, do nothing
      if (err != 'Canceled'){
        PostAlarm('Alm_message',err)
      }
    }
  })


ipcMain.on('disconnect', (e, payload)=>{
  let params = JSON.parse(payload)
  disconnectComms(params)
})

ipcMain.on('connect', (e, payload)=>{
  let params = JSON.parse(payload)
  if(params.controller=='grbl'){
    connectGrbl(params);
  }
  if(params.controller=='plc'){
    connectPlc(params);
  }
})

ipcMain.on('updateGRBL_M_Value', (e, payload)=>{
  let params = JSON.parse(payload)
  if (grbl){
    if (params.id == 'grblXjog'){
      grbl.machine.jogDistance.x = params.val
    }
    if (params.id == 'grblYjog'){
      grbl.machine.jogDistance.y = params.val
    }
    if (params.id == 'grblZjog'){
      grbl.machine.jogDistance.z = params.val
    }
    if (params.id == 'grblXFR'){
      grbl.machine.jogFeedrate.x = params.val
    }
    if (params.id == 'grblYFR'){
      grbl.machine.jogFeedrate.y = params.val
    }
    if (params.id == 'grblZFR'){
      grbl.machine.jogFeedrate.z = params.val
    }
  }
})

const disconnectComms = (params)=>{
  //console.log('params',params)
  if(params.controller=='grbl' && grbl){
    grbl.cleanup();
    grbl = null;
  }
  if(params.controller=='plc' && plc){
    plc.cleanup();
    plc = null;
  }
  if(params.controller =='all'){
    //console.log('disconnect all')
    if (grbl){
      grbl.cleanup();
      grbl = null;
    }
    if (plc){
      plc.cleanup();
      plc = null;
    }
  }
}


const getGRBLSet = (params)=>{
  try{
    if(grbl){
      grbl.sendLine(`$$\n`)

    }else{
      alert(`Not Connected to GRBL Controller`);
    }
  }
  catch(err){
    PostAlarm('Alm_message',err)

  }
}

const connectGrbl = (params)=>{
  if(grbl){
    grbl.cleanup();
    grbl = null; //delete any old connection
  }
  if (params.port){
    grbl = new Grbl(params.port);
    grbl.on('poll', (data)=>
    {
      sendToWindow('grbl-data', data);
    })

    grbl.on('gcode-program-update', (divContent)=>{
      sendToWindow('gcode-program-update', JSON.stringify(divContent))
    })
    grbl.on('gcode-program-finish', ()=>{
      sendToWindow('gcode-program-finish', {});
    })
    grbl.on('gcode-settings-received', (set)=>{
      sendToWindow('gcode-settings-received', set);
    })
    grbl.on('err', (payload)=>
    {
      PostAlarm('Alm_message', payload);
    })
    grbl.connect();
  }else{
    PostAlarm('Alm_message','No port selected for connection to GRBL Controller')
    alert('Please select a GRBL communciation port')
  }
}

const connectPlc = (params)=>
{
  if(plc){
    plc.cleanup();
    plc = null; //delete any old connection
  }
  if (params.port){
    plc = new Plc(params.port);
    plc.on('poll', (data)=>
    {
      sendToWindow('plc-data', data);
    })
    plc.on('err', (payload)=>
    {
      PostAlarm('Alm_message','Error Connecting to the PLC');
      PostAlarm('Alm_message', payload.message);
      //plc = null;
      //plc = null; TODO find error on connect
    })
    plc.connect();
  }else{
    PostAlarm('Alm_message','No port selected for connection to PLC')
    alert('Please select a PLC communciation port')
  }
}

ipcMain.on('grbl-reset', async (e, payload)=>{
  try{
    if(grbl){
      await confirm(`This will abort any current program.  If in motion, position may be lost! Reset CNC controller?`);
      grbl.reset();
      PostAlarm('Alm_message','GRBL Reset Command Issued By Operator')
    }
  }
  catch(err){
    //reject is a cancel, do nothing
    if (err != 'Canceled'){
      PostAlarm('Alm_message',err)
    }
  }
})

ipcMain.on('grbl-home', async (e, payload)=>{
  try{
    if(grbl){
      await confirm(`This will home machine.  Limit switch override needed in PLC`);
      grbl.home();
    }
  }
  catch(err){
    //reject is a cancel, do nothing
    if (err != 'Canceled'){
      PostAlarm('Alm_message',err)
    }
  }
})
 

const confirm = (msg)=>{
  return new Promise((res, rej)=>{
    // Create the browser window.
    popupWindow = new BrowserWindow({
      parent: mainWindow,
      modal: true,
      show: false,
      width: 640,
      height: 480,
      frame: false,
      webPreferences: {
        preload: path.join(__dirname, 'preload.js'),
        nodeIntegration: true
      }
    })

    // and load the index.html of the app.
    popupWindow.loadFile('./public/confirm.html')
    popupWindow.setMenu(null);
    popupWindow.once('ready-to-show', () => {
      popupWindow.show()
      //popupWindow.webContents.openDevTools()
      popupWindow.webContents.send('confirm-msg', msg);
    })
    ipcMain.once('popup-confirm', (e, payload)=>{
      popupWindow.destroy();
      popupWindow = null;
      if(JSON.parse(payload)){
        res();
      } else {
        rej('Canceled');
      }
    })
  })
}

const alert = (msg)=>{
  //return new Promise((res, rej)=>{
    // Create the browser window.
    popupWindow = new BrowserWindow({
      parent: mainWindow,
      modal: true,
      show: false,
      width: 640,
      height: 480,
      frame: false,
      webPreferences: {
        preload: path.join(__dirname, 'preload.js'),
        nodeIntegration: true
      }
    })

    // and load the index.html of the app.
    popupWindow.loadFile('./public/alert.html')
    popupWindow.setMenu(null);
    popupWindow.once('ready-to-show', () => {
      popupWindow.show()
      //popupWindow.webContents.openDevTools()
      popupWindow.webContents.send('alert-msg', msg);
    })
    ipcMain.once('popup-confirm', (e, payload)=>{
      popupWindow.destroy();
      popupWindow = null;
/*       if(JSON.parse(payload)){
        res();
      } else {
        //User acknowleged the message
        rej('Nope, User changed their mind');
      } */
    })
  //})
}

//example of using the confirm as a promise resolution or rejection
//ipcMain.on('confirm', (e, payload)=>{confirm('Do you really want to do something that stupid?')
//  .then(()=>{testPopup({doing: "some shit"});}) //confirmed, do your thang
//  .catch(err=>{});})//reject is a cancel, do nothing
//or as async
ipcMain.on('confirm', async (e, payload)=>{
  try{
    await confirm('Do you really want to do something that stupid?');
    testPopup({doing: "some shit"}); //confirmed, do your thang
  }
  catch(err){
    //reject is a cancel, do nothing
    if (err != 'Canceled'){
      PostAlarm('Alm_message',err)
    }
  }
})

const testPopup = (params)=>{
  console.log(params)
}


const createMainWindow = ()=>{
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1280,
    height: 720,
    frame: false,
    fullscreen: (os.platform() == 'linux'), //set fullscreen on the pi
    //fullscreen: (os.platform() == 'win32'),
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true
    }
  })

  // and load the index.html of the app.
  mainWindow.loadFile('./public/index.html')
  mainWindow.setMenu(null);
  

  // Open the DevTools.
  //mainWindow.webContents.openDevTools()
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createMainWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  mainWindow = null;
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createMainWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.




