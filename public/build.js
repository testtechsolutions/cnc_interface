const background_color = "#24252A"
const button_color= "#303030"
const button_disable_color= "#444444"
const button_3D_color = "black"
const textarea_color_hover= "#D3D3D3"
const ta_background_color= "#C0C0C0"
const settings_background_color= "#444444"
const panel_color= "#303035"
const splitter_color= "#4e4e4e"
const TT_color= "#A00000"
const text_color= "white"


class Widget{
    //basis for all wigets
    constructor(params){
      this.doc = params.doc //hold a ref to the html doc
      this.htmlElement = this.doc.createElement(params.element); //hold the document html element, params. element is "Button", "Select", etc.
      this.parent = params.parent;
      this.id = params.id;
      if(params.id){
        this.htmlElement.id = this.id; //set the doc element id
      }    
      if(params.className){
        this.htmlElement.className = params.className
      }
      this.parent.append(this.htmlElement);
      if (params.abs){
        this.htmlElement.style.position = 'absolute'
        if (params.left){
          this.htmlElement.style.left = params.left
        }
        if (params.bottom){
          this.htmlElement.style.bottom = params.bottom
        }
      }
      if (params.height){
        this.htmlElement.style.height = params.height
      }
      if (params.width){
        this.htmlElement.style.width = params.width
      }
      if (params.margin){
        this.htmlElement.style.margin = params.margin
      }
    }
  }
  
  class Button extends Widget{
    constructor(params){
      params.element = 'BUTTON';
      super(params);
      this.htmlElement.addEventListener("click", ()=>{buttonClick({btn: `${this.id}`,cmd: params.cmd});}); //all buttons should be handled in buttonClick even if they do nothing
      if(params.lbl_text){
        this.htmlElement.innerHTML = params.lbl_text;
      }
      this.htmlElement.style.boxShadow = `3px 3px 0px ${button_3D_color}`
      this.htmlElement.style.outline = 'none'
    }
  }

  
  class Select extends Widget{
    constructor(params){
      params.element = 'SELECT';
      super(params);
      if(params.wid){
        this.htmlElement.style.minWidth = params.wid
      }
      if(params.opt){
        for (var idx in params.opt){
          var option = document.createElement("option");
          option.text = params.opt[idx];
          this.htmlElement.add(option);
        }
        this.htmlElement.style.textAlignLast = "center"
      }

      if (params.id == 'grblPort' && gCommPort){
        buttonClick({btn: 'comm_PortRefresh'});
        this.htmlElement.value = gCommPort
        if (pCommPort){
          pTemp = document.getElementById("plcPort")
          pTemp.value = pCommPort
        }
      }
    }
  }
  
  class Label extends Widget{
    constructor(params){
      params.element = 'Label';
      super(params);
      if(params.lbl_text){
        //add label text
        this.htmlElement.innerHTML = params.lbl_text
        //set label text size
        if (params.fsize){
          this.htmlElement.style.fontSize = params.fsize
        }
        else{
          this.htmlElement.style.fontSize = "24px"
        }
      }
      if(params.event && params.tag){
        emitter.on(params.event, (payload)=>{
        })
      }
      if (params.t_color){
        this.htmlElement.style.color = params.t_color;
      }
      if (params.bold){
        this.htmlElement.style.fontWeight  = 'bold';
      }
    }
  }
  
  class Input extends Widget{
    constructor(params){
      params.element = 'Input';
      super(params);
      this.htmlElement.type = "Text"
      if (params.func){
          if (params.func == "keyup"){
            this.htmlElement.addEventListener("keyup", ()=>{inputEnter(event);}); 
          } else if (params.func == "focusout" && params.funcType == "MachineValue"){
            this.htmlElement.addEventListener("focusout", ()=>{inputChange(params.id);}); 
          }
      }
      if (params.placeholder){
        this.htmlElement.placeholder = params.placeholder
      }
    }
  }
  
  class TextArea extends Widget{
    constructor(params){
      params.element = 'Textarea';
      super(params);
      this.htmlElement.style.outline = 'none'
      if(this.id == "gCodeArea"){
        this.htmlElement.value = gCodeText;
      }
      if(this.id == "Alarm_area"){
        this.htmlElement.value = alarms;
      }
      if (params.readonly){
        this.htmlElement.readOnly = true;
      }
    }
  }
  
  class Image extends Widget{
    constructor(params){
      params.element = 'IMG';
      super(params);
      if(params.img_src){
        this.htmlElement.src = params.img_src
      }
    }
  }
  
  
  class NavBar extends Widget{
    constructor(params){
      params.element = 'Div';
      params.className = 'nav-bb';
      super(params);
      this.panelObj = params.panelObj //ref to the content panel Object
      this.buttons = [];
      this.btnClick = (btn)=>{
        
        //changing colors for nav buttons based on one that is clicked
        if (btn.startsWith("nav-left")){
          for (var key in navBarLeft.buttons) {
            let x = document.getElementById(navBarLeft.buttons[key].id)
            x.className = 'nav-btn'
          }
        }else{
          for (var key in navBarRight.buttons) {
            let x = document.getElementById(navBarRight.buttons[key].id)
            x.className = 'nav-btn'
          }
        }
        let x = document.getElementById(btn)
        x.className = 'nav-btn-active'
  
        let idx = btn.replace(RegExp(`nav-left-`, 'g'), '').replace(RegExp(`nav-right-`, 'g'), '');
        this.panelObj.navTo(idx);
        this.tabSwitch(x.innerHTML)
      }
      this.tabSwitch = (btn)=>{
        //This is where you would add functions when tab clicked on matches
        initialize()
        if (btn == 'CNC'){
          refreshjogSet = false
        }
        if (btn == 'GRBL Settings' && grblConnectedStat){
          buttonClick({btn: 'getGRBLSettings'});
        }


      }
    }
  }
  
  class ContentRow extends Widget{
    constructor(params){
      params.element = 'Div';
      //Passing in classname of DIV where content will reside
      params.className = params.pDIV;
      super(params); 
      if(params.backgroundColor){
        this.htmlElement.style.backgroundColor = settings_background_color;
      }
  
    }
  }
  
  class ScrollWindow extends Widget{
    constructor(params){
      let b_text = 'grblSendCmd'
      params.element = 'DIV';
      params.className = 'scrollable';
      super(params);
      let set = GBRL_SETTINGS['settings']
      for (var idx in set) {
          let row = new ContentRow({doc:this.doc, parent: this.htmlElement, pDIV:'settings-box'});
          new widgets['label']({doc: document, className: 'label-info', id: '', lbl_text: set[idx].lbl_text, parent: row.htmlElement,fsize:"22px"})
          new widgets['input']({doc: document, className: 'display', id: set[idx].id.concat('_VAL') ,placeholder: '????', parent: row.htmlElement, width: '100px'})
          new widgets[set[idx].widget]({doc: document, className: 'inpt_display', id: set[idx].id.concat('_NEWVAL') ,placeholder: '',parent: row.htmlElement,opt:set[idx].opt, width: '100px'})
          new widgets['button']({doc: document, className: 'btn', id: b_text.concat('_').concat(set[idx].id) ,lbl_text: 'Send',parent: row.htmlElement, cmd:set[idx].cmd, width: '100px'})
          //Fills in values for settings that had a saved Current Value
          if (set[idx].cVal){
            if (set[idx].widget == 'select'){
              var n_val = 0
              for (var t in set[idx].opt){
                if (set[idx].cVal === set[idx].opt[t]){
                  n_val=t
                }
                document.getElementById(set[idx].id.concat('_NEWVAL')).selectedIndex = n_val
              }
             
            }else{
              document.getElementById(set[idx].id.concat('_NEWVAL')).value = Number(set[idx].cVal)
            }
          }
      }
      
    }
  }
  
  const widgets = {
    label: Label,
    button: Button,
    select: Select,
    input: Input,
    textarea: TextArea,  
    image: Image,
    scrollwindow: ScrollWindow,
    contentrow: ContentRow,  
  }

  class Panel extends Widget{
    constructor(params){
        params.element = 'Div';
        params.className = params.className;
        var side = params.id.split("-")
        super(params);
        this.newcontent = params.items
        this.content = [] //indexed list of lists of content to place when coresponding btn clicked
        this.clearDiv = ()=>{
          let ta = document.getElementById('gCodeArea')
          if(ta != null){
            gCodeText = ta.value;
          }
          this.saveSettings();
          this.htmlElement.innerHTML = '';
        }
        this.navTo = (idx)=>{
          if(this.newcontent[idx]){
            this.clearDiv();
            this.addContent(idx);
          }
        }
        this.addContent = (idx)=>{
          if(idx >= this.newcontent.length){
            return;
          }
          let page = this.newcontent[idx];
          for(let rowIdx in page){
              page[rowIdx].params.parent = this.htmlElement
              new widgets[page[rowIdx].widget](page[rowIdx].params)
              if (page[rowIdx].params.id == 'grblPort' && gCommPort){
                this.htmlElement.selectedIndex = 0
              }
          }
        }
        this.saveSettings = ()=>{
          //Save settings on the screen before switching tabs
          let set = GBRL_SETTINGS['settings']
          for (var idx in set) {
            if (document.getElementById(set[idx].id.concat('_NEWVAL'))){
              set[idx].cVal = document.getElementById(set[idx].id.concat('_NEWVAL')).value
            }
          }
        }
      }
    }
  class Background {
    constructor(params){
      params.element = params.parent;
      for(let wigIDx in params.items){
          params.items[wigIDx].params.parent = params.parent
          let wig = new widgets[params.items[wigIDx].widget](params.items[wigIDx].params)
      }
    }
  }
  
  /// ---------  ///
  const LowerindexWidgets = [
    {widget: "input", params: {doc: document, className: 'single-command', id: 'singleLineEntry', lbl_text: '',
                               placeholder:'Enter command',fsize:'30px', abs: true,left:"5px",bottom:'5px',height:'60px',width:'1125px',func:"keyup"}},
    {widget: "button", params: {doc: document, className: 'btn', id: 'singleLineSend', lbl_text: 'Send', abs: true,left:'1140px',
                              bottom:'0.5px',width:'100px',height: '60px',img_src: ""}},
  ]
  const Lowerindex = new Background({doc: document, parent: document.getElementById("lower"), id: 'lower', className: 'index-lower',items:LowerindexWidgets});
  /// ---------  ///
  const footerWidgets = [
        {widget: "image", params: {doc: document, className: 'btn', id: '', lbl_text: 'Resume', abs: true , img_src: "./images/Logo5_noName.png" , 
                                  left:'10px',bottom:'15px',height:'40px'}},
        {widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Universal CNC Software',fsize:'30px', abs: true,
                                  left:"500px",bottom:'10px',height:'34px'}},
        {widget: "button", params: {doc: document, className: 'btn', id: 'exit', lbl_text: '<img src="./images/Exit.png" />', abs: true,left:'1170px',bottom:'0.5px',width:'100px'}},
  ]
  const panelFooter = new Background({doc: document, parent: document.getElementById("footer"), id: 'footer', className: 'index-footer',items:footerWidgets});
  /// ---------  ///

 
  const panelLeftWidgets = {
    0:{       
        //row 0//
          0:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'PLC',fsize:'', abs: true,left:"70px",bottom:'405px',height:'34px'}},
          1:{widget: "button", params: {doc: document, className: 'btn', id: 'plcConnect', lbl_text: 'Connect', abs: true,left:'150px',bottom:'405px',width:'150px'}},
          2:{widget: "button", params: {doc: document, className: 'btn', id: 'plcDisconnect', lbl_text: 'Disconnect', abs: true,left:'310px',bottom:'405px',width:'150px'}},
      //row 1//
          3:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'PORT',fsize:'', abs: true,left:"70px",bottom:'340px',height:'34px',width:'50px'}},
          4:{widget: "select", params: {doc: document, className: 'btn', id: 'plcPort', lbl_text: '', abs: true,left:'150px',bottom:'340px',width:'225px',height:'50px'}},
          5:{widget: "button", params: {doc: document, className: 'btn', id: 'comm_PortRefresh', lbl_text: `<img src="./images/Refresh.png" alt="logo"/>`, abs: true,left:'390px',bottom:'340px',height:'50px',width:'50px'}},         
      //row 2//
          6:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'GRBL',fsize:'', abs: true,left:"70px",bottom:'260px',height:'34px',width:'50px'}},
          7:{widget: "button", params: {doc: document, className: 'btn', id: 'grblConnect', lbl_text: 'Connect', abs: true,left:'150px',bottom:'260px',width:'150px'}},
          8:{widget: "button", params: {doc: document, className: 'btn', id: 'grblDisconnect', lbl_text: 'Disconnect', abs: true,left:'310px',bottom:'260px',width:'150px'}},
      //row 3//
          9:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'PORT',fsize:'', abs: true,left:"70px",bottom:'195px',height:'34px',width:'50px'}},
          10:{widget: "select", params: {doc: document, className: 'btn', id: 'grblPort', lbl_text: '', abs: true,left:'150px',bottom:'195px',width:'225px',height:'50px'}},
          11:{widget: "button", params: {doc: document, className: 'btn', id: 'comm_PortRefresh', lbl_text: `<img src="./images/Refresh.png" alt="logo"/>`, abs: true,left:'390px',bottom:'195px',height:'50px',width:'50px'}},  
            },
    1:{
      //row 0//
          0:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Lockout',fsize:'',bold:false, abs: true,left:"10px",bottom:'405px',height:'34px',width:'140px'}},
          1:{widget: "button", params: {doc: document, className: 'btn', id: 'plcLockoutRst', lbl_text: 'Trip Reset', abs: true,left:'160px',bottom:'405px',height:'50px',width:'270px'}},
          2:{widget: "input", params: {doc: document, className: 'display', id: 'plcLockoutState', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"440px",bottom:'405px',height:'50px',width:'130px'}},
      //row 1//
          3:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Limit Switch',fsize:'',bold:false, abs: true,left:"10px",bottom:'330px',height:'34px',width:'140px',t_color:''}},
          4:{widget: "button", params: {doc: document, className: 'btn', id: 'plcLimitOverride', lbl_text: 'Override', abs: true,left:'160px',bottom:'330px',width:'130px',height:'50px'}},
          5:{widget: "button", params: {doc: document, className: 'btn', id: 'plcToolIn', lbl_text: 'Tool In', abs: true,left:'300px',bottom:'330px',width:'130px',height:'50px'}},
          6:{widget: "button", params: {doc: document, className: 'btn', id: 'plcToolOut', lbl_text: 'Tool Out', abs: true,left:'440px',bottom:'330px',width:'130px',height:'50px'}},
      //row 2//
          7:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Brakes',fsize:'',bold:false, abs: true,left:"10px",bottom:'255px',height:'34px',width:'140px',t_color:''}},
          8:{widget: "button", params: {doc: document, className: 'btn', id: 'plcBrakeOff', lbl_text: 'Off', abs: true,left:'160px',bottom:'255px',width:'130px',height:'50px'}},
          9:{widget: "button", params: {doc: document, className: 'btn', id: 'plcBrakeOn', lbl_text: 'On', abs: true,left:'300px',bottom:'255px',width:'130px',height:'50px'}},
          10:{widget: "input", params: {doc: document, className: 'display', id: 'plcBrakeState', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"440px",bottom:'255px',height:'50px',width:'130px'}},
      //row 3//
          11:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Coolant',fsize:'',bold:false, abs: true,left:"10px",bottom:'180px',height:'34px',width:'140px',t_color:''}},
          12:{widget: "button", params: {doc: document, className: 'btn', id: 'grblCoolantOff', lbl_text: 'Off', abs: true,left:'160px',bottom:'180px',width:'130px',height:'50px'}},
          13:{widget: "button", params: {doc: document, className: 'btn', id: 'grblCoolantOn', lbl_text: 'On', abs: true,left:'300px',bottom:'180px',width:'130px',height:'50px'}},
          14:{widget: "input", params: {doc: document, className: 'display', id: 'plcCoolantState', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"440px",bottom:'180px',height:'50px',width:'130px'}},     
      //row 4//
          15:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Spindle',fsize:'',bold:false, abs: true,left:"10px",bottom:'105px',height:'40px',width:'140px',t_color:''}},
          16:{widget: "button", params: {doc: document, className: 'btn', id: 'plcSpindleStop', lbl_text: 'Stop', abs: true,left:'160px',bottom:'105px',width:'130px',height:'50px'}},
          17:{widget: "button", params: {doc: document, className: 'btn', id: 'plcSpindleRun', lbl_text: 'Run', abs: true,left:'300px',bottom:'105px',width:'130px',height:'50px'}},
          18:{widget: "input", params: {doc: document, className: 'display', id: 'plcSpindleState', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"440px",bottom:'105px',height:'50px',width:'130px'}},   
      //row 5//
          19:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Direction',fsize:'',bold:false, abs: true,left:"10px",bottom:'30px',height:'34px',width:'140px',t_color:''}},
          20:{widget: "button", params: {doc: document, className: 'btn', id: 'plcSpindleFwd', lbl_text: 'Fwd', abs: true,left:'160px',bottom:'30px',width:'130px',height:'50px'}},
          21:{widget: "button", params: {doc: document, className: 'btn', id: 'plcSpindleRev', lbl_text: 'Rev', abs: true,left:'300px',bottom:'30px',width:'130px',height:'50px'}},
          22:{widget: "input", params: {doc: document, className: 'display', id: 'plcSpindleDir', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"440px",bottom:'30px',height:'50px',width:'130px'}},   
      },
    2:{
      //row 0//
          23:{widget: "button", params: {doc: document, className: 'btn', id: 'grblRun', lbl_text: 'Run', abs: true,left:'20px',bottom:'410px',width:'100px',height:'50px'}},
          24:{widget: "button", params: {doc: document, className: 'btn', id: 'grblHome', lbl_text: 'Home', abs: true,left:'130px',bottom:'410px',width:'100px',height:'50px'}},
          25:{widget: "button", params: {doc: document, className: 'btn', id: 'grblReset', lbl_text: 'Reset', abs: true,left:'240px',bottom:'410px',width:'100px',height:'50px'}},
          26:{widget: "button", params: {doc: document, className: 'btn', id: 'openGcodeFile', lbl_text: 'Open', abs: true,left:'350px',bottom:'410px',width:'100px',height:'50px'}},
          27:{widget: "button", params: {doc: document, className: 'btn', id: 'saveGcodeFile', lbl_text: 'Save', abs: true,left:'460px',bottom:'410px',width:'100px',height:'50px'}},
      //row 1//
          28:{widget: "textarea", params: {doc: document, className: 'textarea-container', id: 'gCodeArea', lbl_text: '', abs: true,left:'0px',bottom:'0px',width:'600px',height:'405px'}},
      },
      3:{
        //row 0//
            0:{widget: "button", params: {doc: document, className: 'btn', id: 'templateCode', lbl_text: 'Generate Code', abs: true,left:'190px',bottom:'410px',width:'250px',height:'50px'}},
        //row 1//
            1:{widget: "button", params: {doc: document, className: 'btn', id: 'templateCircle', lbl_text: 'Circle Pocket', abs: true,left:'30px',bottom:'350px',width:'200px',height:'50px'}},
        //row 2//
            2:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'depth',fsize:'',bold:false, abs: true,left:"30px",bottom:'290px',height:'34px',width:'120px',t_color:''}},
            3:{widget: "input", params: {doc: document, className: 'inpt_display', id: 'pktDepth', lbl_text: '',placeholder:'0',fsize:'', abs: true,left:"160px",bottom:'290px',height:'50px',width:'130px',func:""}},
            4:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'radius',fsize:'',bold:false, abs: true,left:"300px",bottom:'290px',height:'34px',width:'120px',t_color:''}},
            5:{widget: "input", params: {doc: document, className: 'inpt_display', id: 'pktradius', lbl_text: '',placeholder:'0',fsize:'', abs: true,left:"430px",bottom:'290px',height:'50px',width:'130px',func:""}},  
        //row 3//
            6:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Feedrate',fsize:'',bold:false, abs: true,left:"30px",bottom:'230px',height:'34px',width:'120px',t_color:''}},
            7:{widget: "input", params: {doc: document, className: 'inpt_display', id: 'pktFR', lbl_text: '',placeholder:'0',fsize:'', abs: true,left:"160px",bottom:'230px',height:'50px',width:'130px',func:""}},
            8:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Cutter Size',fsize:'',bold:false, abs: true,left:"300px",bottom:'230px',height:'34px',width:'120px',t_color:''}},
            9:{widget: "input", params: {doc: document, className: 'inpt_display', id: 'pktCutterSize', lbl_text: '',placeholder:'0',fsize:'', abs: true,left:"430px",bottom:'230px',height:'50px',width:'130px',func:""}},
        //row 3//
            10:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Multiple Pass',fsize:'',bold:false, abs: true,left:"30px",bottom:'170px',height:'34px',width:'120px',t_color:''}},
            11:{widget: "select", params: {doc: document, className: 'inpt_display', id: 'multiPass', lbl_text: '',placeholder:'0',fsize:'', abs: true,left:"160px",bottom:'170px',height:'50px',width:'130px',func:"",opt:{0:'No',1:'Yes'}, default:'No', cVal: "" }},
            12:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Cutter Size',fsize:'',bold:false, abs: true,left:"300px",bottom:'170px',height:'34px',width:'120px',t_color:''}},
            13:{widget: "input", params: {doc: document, className: 'inpt_display', id: 'pktCutterSize', lbl_text: '',placeholder:'0',fsize:'', abs: true,left:"430px",bottom:'170px',height:'50px',width:'130px',func:""}},  
        },
}
  
  const panelLeft    = new Panel({doc: document, parent: document.getElementById("upper-section"), id: 'content-left', className: 'upper-panel',abs:true,left:'0px',bottom:'155px',width:'615px',items:panelLeftWidgets,abs:true,left:'0px',bottom:'155px'});
  const navBarLeft   = new NavBar({doc: document, parent: document.getElementsByTagName("header")[0],panelObj: panelLeft, id: 'nav-left'})
  navBarLeft.buttons = [new Button({doc: document, parent: navBarLeft.htmlElement, className: 'nav-btn', id: 'nav-left-0', lbl_text: 'Comm'}),
                        new Button({doc: document, parent: navBarLeft.htmlElement, className: 'nav-btn', id: 'nav-left-1', lbl_text: 'Control'}),
                        new Button({doc: document, parent: navBarLeft.htmlElement, className: 'nav-btn', id: 'nav-left-2', lbl_text: 'Program'}),
                        //new Button({doc: document, parent: navBarLeft.htmlElement, className: 'nav-btn', id: 'nav-left-3', lbl_text: 'Templates'})
  ]
  
const panelRightWidgets = {
      0:{       
          //row 0//
            0:{widget: "button", params: {doc: document, className: 'btn', id: 'grblResume', lbl_text: 'Resume', abs: true,left:'45px',bottom:'405px',width:'120px'}},
            1:{widget: "button", params: {doc: document, className: 'btn', id: 'grblHold', lbl_text: 'Hold', abs: true,left:'185px',bottom:'405px',width:'120px'}},
            2:{widget: "input", params: {doc: document, className: 'display', id: 'grblState', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"325px",bottom:'405px',height:'50px',width:'120px'}},
            35:{widget: "input", params: {doc: document, className: 'display', id: 'ReportInches_VAL', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"465px",bottom:'405px',height:'50px',width:'120px'}},
            //row 1//
            3:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: `X\n(abs)`,fsize:'', abs: true,left:"10px",bottom:'350px',height:'34px',width:'50px'}},
            4:{widget: "input", params: {doc: document, className: 'display', id: 'grblXposAbs', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"75px",bottom:'335px',height:'50px',width:'120px'}},
            5:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: `Y\n(abs)`,fsize:'', abs: true,left:"205px",bottom:'350px',height:'34px',width:'30px'}},
            6:{widget: "input", params: {doc: document, className: 'display', id: 'grblYposAbs', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"270px",bottom:'335px',height:'50px',width:'120px'}},
            7:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: `Z\n(abs)`,fsize:'', abs: true,left:"400px",bottom:'350px',height:'34px',width:'30px'}},
            8:{widget: "input", params: {doc: document, className: 'display', id: 'grblZposAbs', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"465px",bottom:'335px',height:'50px',width:'120px'}},
            //row 2//
            9:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: `X\n(wco)`,fsize:'', abs: true,left:"10px",bottom:'280px',height:'34px',width:'50px'}},
            10:{widget: "input", params: {doc: document, className: 'display', id: 'grblXpos', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"75px",bottom:'265px',height:'50px',width:'120px'}},
            11:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: `Y\n(wco)`,fsize:'', abs: true,left:"205px",bottom:'280px',height:'34px',width:'50px'}},
            12:{widget: "input", params: {doc: document, className: 'display', id: 'grblYpos', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"270px",bottom:'265px',height:'50px',width:'120px'}},
            13:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: `Z\n(wco)`,fsize:'', abs: true,left:"400px",bottom:'280px',height:'34px',width:'50px'}},
            14:{widget: "input", params: {doc: document, className: 'display', id: 'grblZpos', lbl_text: '',placeholder:'-Unknown-',fsize:'', abs: true,left:"465px",bottom:'265px',height:'50px',width:'120px'}},
            //row 3//
            15:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Feedrate',fsize:'', abs: true,left:"250px",bottom:'195px',height:'34px'}},
            16:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Distance',fsize:'', abs: true,left:"350px",bottom:'195px',height:'34px',width:'100px'}},
            //row 4//
            17:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: `X Jog`,fsize:'', abs: true,left:"10px",bottom:'165px',height:'34px',width:'50px'}},
            18:{widget: "button", params: {doc: document, className: 'btn', id: 'X-Jog', lbl_text: '<img src="./images/Left.png" />', abs: true,left:'80px',bottom:'145px',width:'35px',height:'60px'}},
            19:{widget: "button", params: {doc: document, className: 'btn', id: 'X+Jog', lbl_text: '<img src="./images/Right.png" />', abs: true,left:'160px',bottom:'145px',width:'35px',height:'60px' }},
            20:{widget: "input", params: {doc: document, className: 'inpt_display', id: 'grblXFR', lbl_text: '',placeholder:'0',fsize:'', abs: true,left:"240px",bottom:'145px',height:'50px',width:'110px',func:"focusout", funcType:"MachineValue"}},
            21:{widget: "input", params: {doc: document, className: 'inpt_display', id: 'grblXjog', lbl_text: '',placeholder:'0',fsize:'', abs: true,left:"360px",bottom:'145px',height:'50px',width:'110px',func:"focusout", funcType:"MachineValue"}},
            22:{widget: "button", params: {doc: document, className: 'btn', id: 'grblzeroX', lbl_text: 'Zero', abs: true,left:'480px',bottom:'145px',width:'100px'}},
        //row 5//
            23:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: `Y Jog`,fsize:'', abs: true,left:"10px",bottom:'95px',height:'34px',width:'50px'}},
            24:{widget: "button", params: {doc: document, className: 'btn', id: 'Y-Jog', lbl_text: `<img src="./images/Down.png" />`, abs: true,left:'80px',bottom:'75px',width:'35px',height:'60px'}},
            25:{widget: "button", params: {doc: document, className: 'btn', id: 'Y+Jog', lbl_text: '<img src="./images/Up.png" />', abs: true,left:'160px',bottom:'75px',width:'35px',height:'60px'}},
            26:{widget: "input", params: {doc: document, className: 'inpt_display', id: 'grblYFR', lbl_text: '',placeholder:'0',fsize:'', abs: true,left:"240px",bottom:'75px',height:'50px',width:'110px',func:"focusout", funcType:"MachineValue"}},
            27:{widget: "input", params: {doc: document, className: 'inpt_display', id: 'grblYjog', lbl_text: '',placeholder:'0',fsize:'', abs: true,left:"360px",bottom:'75px',height:'50px',width:'110px',func:"focusout", funcType:"MachineValue"}},
            28:{widget: "button", params: {doc: document, className: 'btn', id: 'grblzeroY', lbl_text: 'Zero', abs: true,left:'480px',bottom:'75px',width:'100px'}}, 
        //row 6//
            29:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: `Z Jog`,fsize:'', abs: true,left:"10px",bottom:'25px',height:'34px',width:'50px'}},
            30:{widget: "button", params: {doc: document, className: 'btn', id: 'Z-Jog', lbl_text: `<img src="./images/Down.png" />`, abs: true,left:'80px',bottom:'5px',width:'35px',height:'60px'}},
            31:{widget: "button", params: {doc: document, className: 'btn', id: 'Z+Jog', lbl_text: '<img src="./images/Up.png" />', abs: true,left:'160px',bottom:'5px',width:'35px',height:'60px'}},
            32:{widget: "input", params: {doc: document, className: 'inpt_display', id: 'grblZFR', lbl_text: '',placeholder:'0',fsize:'', abs: true,left:"240px",bottom:'5px',height:'50px',width:'110px',func:"focusout", funcType:"MachineValue"}},
            33:{ widget: "input", params: {doc: document, className: 'inpt_display', id: 'grblZjog', lbl_text: '',placeholder:'0',fsize:'', abs: true,left:"360px",bottom:'5px',height:'50px',width:'110px',func:"focusout", funcType:"MachineValue"}},
            34:{widget: "button", params: {doc: document, className: 'btn', id: 'grblzeroZ', lbl_text: 'Zero', abs: true,left:'480px',bottom:'5px',width:'100px'}}, 
              },
      1:{
        //row 0//
            0:{widget: "button", params: {doc: document, className: 'btn', id: 'openGRBLSettingsFile', lbl_text: 'Open', abs: true,left:'40px',bottom:'405px',width:'100px',height:'60px'}},
            1:{widget: "button", params: {doc: document, className: 'btn', id: 'saveGRBLsettings', lbl_text: 'Save Current', abs: true,left:'150px',bottom:'405px',width:'180px',height:'60px'}},
            2:{widget: "button", params: {doc: document, className: 'btn', id: 'grblSendAll', lbl_text: 'SendAll', abs: true,left:'340px',bottom:'405px',width:'120px',height:'60px'}},
            3:{widget: "button", params: {doc: document, className: 'btn', id: 'getGRBLSettings', lbl_text: `<img src="./images/Refresh.png" alt="logo"/>`, abs: true,left:'470px',bottom:'405px',width:'100px',height:'60px'}},
        //row 1//
            4:{widget: "contentrow", params: {doc: document, className: 'hrow', id: '', lbl_text: '',backgroundColor:'gray', abs: true,left:'11px',bottom:'345px',width:'591px',height:'40px'}},
            5:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Description',fsize:'16px',bold:true, abs: true,left:"80px",bottom:'325px',height:'40px',width:'140px',t_color:'black'}},
            6:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Current Value',fsize:'16px',bold:true, abs: true,left:"220px",bottom:'325px',height:'40px',width:'140px',t_color:'black'}},            
            7:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'New Value',fsize:'16px',bold:true, abs: true,left:"330px",bottom:'325px',height:'40px',width:'140px',t_color:'black'}},            
            8:{widget: "label", params: {doc: document, className: 'label-info', id: '', lbl_text: 'Send New Value',fsize:'16px',bold:true, abs: true,left:"440px",bottom:'325px',height:'40px',width:'140px',t_color:'black'}},            
        //row 2//
            9:{widget: "scrollwindow", params: {doc: document, className: 'scrollable', id: '', lbl_text: '', abs: true,left:'10px',bottom:'10px',width:'590px',height:'330px'}},
             },
      2:{
        //row 0//
            0:{widget: "textarea", params: {doc: document, className: 'alarmarea-container', id: 'Alarm_area', lbl_text: '', abs: true,left:'0px',bottom:'0px',width:'600px',height:'465px', readonly:true}},
             },
  }

  
  const panelRight    = new Panel({doc: document, parent: document.getElementById("upper-section"), id: 'content-right', className: 'upper-panel',items:panelRightWidgets,abs:true,left:'635px',bottom:'155px',width:'615px'});
  const navBarRight   = new NavBar({doc: document, parent: document.getElementsByTagName("header")[0],panelObj: panelRight,  id: 'nav-right'})
  navBarRight.buttons = [new Button({doc: document, parent: navBarRight.htmlElement, className: 'nav-btn', id: 'nav-right-0', lbl_text: `CNC`}),
                        new Button({doc: document, parent: navBarRight.htmlElement, className: 'nav-btn', id: 'nav-right-1', lbl_text: 'GRBL Settings'}),
                        new Button({doc: document, parent: navBarRight.htmlElement, className: 'nav-btn', id: 'nav-right-2', lbl_text: 'Alarms'})
  ];  
  
  
  const GBRL_SETTINGS = {
    settings: {
//      'Header':  ['Description','Current Value','New Value','Send New Val'],
      1:  { widget: "input" , cmd: '0', id: "StepPulse", lbl_text: "Step Pulse (ms):", default:10, cVal: "" },
      2:  { widget: "input" , cmd: '1', id: "StepIdleDelay", lbl_text: "Step Idle Delay (ms):", default:25, cVal: "" },
      3:  { widget: "select", cmd: '2', id: "StepPortInvert", lbl_text: "Step Port Invert:", opt:{0:'None',1:'X',2:'Y',3:'X,Y',4:'Z',5:' X,Z',6:'Y,Z',7:'X,Y,Z'}, default:'None', cVal: "" },
      4:  { widget: "select", cmd: '3',  id: "DirectionPortInvert", lbl_text: "Dir. Port Invert:", opt:{0:'None',1:'X',2:'Y',3:'X,Y',4:'Z',5:' X,Z',6:'Y,Z',7:'X,Y,Z'}, default:'None', cVal: "" },
      5:  { widget: "select", cmd: '4',  id: "StepEnableInvert", lbl_text: "Step Enable Invert:", opt:{0:'False',1:'True'}, default:'False', cVal: "" },
      6:  { widget: "select", cmd: '5',  id: "LimitPinInvert", lbl_text: "Limit Pin Invert:", opt:{0:'False',1:'True'}, default:'False', cVal: "" },
      7:  { widget: "select", cmd: '6',  id: "ProbePin Invert", lbl_text: "Probe Pin Invert:", opt:{0:'False',1:'True'}, default:'False', cVal: "" },
      8:  { widget: "select", cmd: '10',  id: "StatusReport", lbl_text: "Status Report:", opt:{0:'wPos',1:'mPos',2:'P_Buf,4',3:'S_Buf',4:'H_Lim',5:'P_Pin',6:'C_Pin'}, default:1, cVal: "" },
      9:  { widget: "input" , cmd: '11',  id: "JunctionDeviation", lbl_text: "Junct. Deviation (mm):", default:0.010, cVal: "" },
      10: { widget: "input" , cmd: '12',  id: "ArcTolerance", lbl_text: "Arc Tolerance (mm):", default:0.002, cVal: "" },
      11: { widget: "select", cmd: '13',  id: "ReportInches", lbl_text: "Position Report:", opt:{0:'mm',1:'inch'}, default:'mm', cVal: "" },
      12: { widget: "select", cmd: '20',   id: "SoftLimit", lbl_text: "Soft Limits Enabled:", opt:{0:'False',1:'True'}, default:'False', cVal: "" },
      13: { widget: "select", cmd: '21',   id: "HardLimit", lbl_text: "Hard Limits Enabled:", opt:{0:'False',1:'True'}, default:'False', cVal: "" },
      14: { widget: "select", cmd: '22',   id: "HomingCycle", lbl_text: "Homing Cycle Enabled:", opt:{0:'False',1:'True'}, default:'True', cVal: "" },
      15: { widget: "select", cmd: '23',   id: "HomingDirInvert", lbl_text: "Homing Dir. Invert:", opt:{0:'False',1:'True'}, default:'False', cVal: "" },
      16: { widget: "input" , cmd: '24',   id: "HomingFeed", lbl_text: "Homing Feed (mm/min):", default:25, cVal: "" },
      17: { widget: "input" , cmd: '25',   id: "HomingSeek", lbl_text: "Homing Seek (mm/min):", default:500, cVal: "" },
      18: { widget: "input" , cmd: '26',   id: "HomingDebounce", lbl_text: "Homing Debounce (ms):", default:250, cVal: "" },
      19: { widget: "input" , cmd: '27',   id: "HomingPullOff", lbl_text: "Homing Pull Off (ms):", default:1.0, cVal: "" },
      //20: { widget: "input" , cmd: '30',   id: "MaxSpindle", lbl_text: "Max Spdl Speed (RPM):", default:1000, cVal: "" },
      //21: { widget: "input" , cmd: '31',   id: "MinSpindle", lbl_text: "Min Spdl Speed (RPM):", default:0, cVal: "" },
      //22: { widget: "select", cmd: '32',   id: "LaserMode", lbl_text: "Laser Mode Enabled:", opt:{0:'False',1:'True'}, default:'False', cVal: "" },
      23: { widget: "input" , cmd: '100',   id: "XStep", lbl_text: "X-Step (Steps/mm):", default:250, cVal: "" },
      24: { widget: "input" , cmd: '101',   id: "YStep", lbl_text: "Y-Step (Steps/mm):", default:250, cVal: "" },
      25: { widget: "input" , cmd: '102',   id: "ZStep", lbl_text: "Z-Step (Steps/mm):", default:250, cVal: "" },
      26: { widget: "input" , cmd: '110',   id: "XMaxRate", lbl_text: "X-Max Rate (mm/min):", default:500, cVal: "" },
      27: { widget: "input" , cmd: '111',   id: "YMaxRate", lbl_text: "Y-Max Rate (mm/min):", default:500, cVal: "" },
      28: { widget: "input" , cmd: '112',   id: "ZMaxRate", lbl_text: "Z-Max Rate (mm/min):", default:500, cVal: "" },
      29: { widget: "input" , cmd: '120',   id: "XAcc", lbl_text: "X-Accel (mm/sec^2):", default:10, cVal: "" },
      30: { widget: "input" , cmd: '121',   id: "YAcc", lbl_text: "Y-Accel (mm/sec^2):", default:10, cVal: "" },
      31: { widget: "input" , cmd: '122',   id: "ZAcc", lbl_text: "Z-Accel (mm/sec^2):", default:10, cVal: "" },
      32: { widget: "input" , cmd: '130',   id: "XMaxTravel", lbl_text: "X-Max Travel (mm):", default:200, cVal: "" },
      33: { widget: "input" , cmd: '131',   id: "YMaxTravel", lbl_text: "Y-Max Travel (mm):", default:200, cVal: "" },
      34: { widget: "input" , cmd: '132',   id: "ZMaxTravel", lbl_text: "Z-Max Travel (mm):", default:200, cVal: "" },
    },
    lookup:{ //cmd to widget lookup 
      0 : 1,
      1 : 2,
      2 : 3,
      3 : 4,
      4 : 5,
      5 : 6,
      6 : 7,
      10 : 8,
      11 : 9,
      12 : 10,
      13 : 11,
      20 : 12,
      21 : 13,
      22 : 14,
      23 : 15,
      24 : 16,
      25 : 17,
      26 : 18,
      27 : 19,
      100 : 23,
      101 : 24,
      102 : 25,
      110 : 26,
      111 : 27,
      112 : 28,
      120 : 29,
      121 : 30,
      122 : 31,
      130 : 32,
      131 : 33,
      132 : 34
    }
  }