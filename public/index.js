const electron = require('electron');
const EventEmitter = require( 'events' );
const {ipcRenderer} = electron;
let gCodeText = ""; //global ref to the gCode text area
let gCommPort = ""; //global ref to grbl comm port name
let pCommPort = ""; //global ref to plc comm port name
let alarms = [] //gloabal ref for alarms
let refreshjogSet = false
let grblConnectedStat = false

//////////////////////////////////////////////////////////////////add pre-configured patterns


ipcRenderer.on('gcode-program-update', (e, payload)=>{
  let text = JSON.parse(payload);  
  let ta = document.getElementById('gCodeArea');
  if(ta==null){
    return;
  }
  if(ta.className != "gcode-div"){
    gCodeText = ta.value;
    let parent = ta.parentElement
    parent.removeChild(ta)
    ta = document.createElement('div')
    ta.className = "gcode-div"
    ta.id = "gCodeArea"
    parent.appendChild(ta)
    ta.style.position = 'absolute'
    ta.style.left = '0px'
    ta.style.bottom = '0px'
    ta.style.height = '400px'
    ta.style.width = '600px'
  }
  ta.innerHTML = text;
})

ipcRenderer.on('gcode-program-finish', (e, payload)=>{  
  let ta = document.getElementById('gCodeArea');
  if(ta==null){
    return;
  }
  if(ta.className != "textarea-container"){
    let parent = ta.parentElement
    parent.removeChild(ta)
    ta = document.createElement('textarea')
    ta.className = "textarea-container"
    ta.id = "gCodeArea"
    parent.appendChild(ta)
    ta.value = gCodeText;
    ta.style.position = 'absolute'
    ta.style.left = '0px'
    ta.style.bottom = '0px'
    ta.style.height = '405px'
    ta.style.width = '600px'
  }
})

ipcRenderer.on('gcode-settings-received',(e,payload)=>{ 
  let sett = GBRL_SETTINGS['settings'][GBRL_SETTINGS['lookup'][payload["cmd"]]]
  if(sett == undefined){
    return
  }
  if (sett.widget == 'select'){
    if(document.getElementById(sett.id.concat('_VAL'))){
      document.getElementById(sett.id.concat('_VAL')).value = sett.opt[payload['val']]
    }
    }else{
      if(document.getElementById(sett.id.concat('_VAL'))){
        document.getElementById(sett.id.concat('_VAL')).value = payload['val']
      }
    }
})

ipcRenderer.on('Alm_message', (e, msg)=>{
  //pass alarm messages from backend to front
  addAlarm(msg)
})


ipcRenderer.on('gcode-load', (e, payload)=>{
  gCodeText = JSON.parse(payload);
  let ta = document.getElementById('gCodeArea');
  if(ta != null){
    ta.value = gCodeText;
  }
})

ipcRenderer.on('GRBLSettings-load', (e, payload)=>{
  settingsText = JSON.parse(payload);
  let sett = GBRL_SETTINGS['settings']
  for (var idx in sett) {
    if (sett[idx].lbl_text){
      if (sett[idx].widget == 'select'){
        var n_val = 0
        for (var t in sett[idx].opt){
          if (settingsText[sett[idx].lbl_text.replace(":","")] === sett[idx].opt[t]){
            n_val=t
          }
          document.getElementById(sett[idx].id.concat('_NEWVAL')).selectedIndex = n_val
        }
       
      }else{
        document.getElementById(sett[idx].id.concat('_NEWVAL')).value = settingsText[sett[idx].lbl_text.replace(":","")]
      }
    }
  }
})


ipcRenderer.on('plc-data', (e, payload)=>{
  let tags = JSON.parse(payload);
  setValue("plcLockoutState", (tags['LCK_OUT_OK']) ? 'Ok' : 'Tripped');
  setValue("plcCoolantState", (tags['COOL_ON']) ? 'On' : 'Off');
  setValue("plcBrakeState", (tags['BRK_ON']) ? 'On' : 'Off');
  setValue("plcSpindleDir", (tags['SPDL_DIR_STATE']) ? 'Reverse' : 'Forward');
  setValue("plcSpindleState", (tags['SPDL_RUN_STATE']) ? 'Running' : 'Stopped');
})


ipcRenderer.on('com-ports', (e, ports)=>{
  let selects = {plcSelect: document.getElementById("plcPort"),
                grblSelect: document.getElementById("grblPort")}
  if (selects['plcSelect'] != null){
    for(let s in selects){
      while(selects[s].options.length){
        selects[s].options.remove(0)
      }
      for(let p in ports){
        let opt = document.createElement("option")
        opt.value = ports[p].path;
        opt.innerHTML = `${ports[p].path}: ${ports[p].manufacturer}`;
        selects[s].options.add(opt)
      }
    }
  }
  if (gCommPort){
    gTemp = document.getElementById("grblPort")
    gTemp.value = gCommPort
  }
  if (pCommPort){
    pTemp = document.getElementById("plcPort")
    pTemp.value = pCommPort
  }
})



ipcRenderer.on('grbl-data', (e, payload)=>
{
  let machine = JSON.parse(payload);
  //console.log('payload',payload)
  grblConnectedStat = machine.connectionStatus
  setDisable('grblPort', machine.connectionStatus)
  setDisable('grblConnect', machine.connectionStatus)
  setDisable('grblDisconnect', !machine.connectionStatus)
  setDisable('X-Jog', !machine.connectionStatus)
  setDisable('X+Jog', !machine.connectionStatus)
  setDisable('Y-Jog', !machine.connectionStatus)
  setDisable('Y+Jog', !machine.connectionStatus)
  setDisable('Z-Jog', !machine.connectionStatus)
  setDisable('Z+Jog', !machine.connectionStatus)
  setDisable('grblzeroX', !machine.connectionStatus)
  setDisable('grblzeroY', !machine.connectionStatus)
  setDisable('grblzeroZ', !machine.connectionStatus)
  setDisable('grblResume', !machine.connectionStatus)
  setDisable('grblHold', !machine.connectionStatus)
  setDisable('singleLineSend', !machine.connectionStatus || !(machine.state=='Idle'))
  setDisable('saveGRBLsettings', !machine.connectionStatus)
  setDisable('grblSendAll', (!machine.connectionStatus || !(machine.state=='Idle')))
  setDisable('getGRBLSettings', !machine.connectionStatus)
  setDisable('grblRun', !machine.connectionStatus)
  setDisable('grblReset', !machine.connectionStatus)
  const SPINDLE_STATES = ['Off', 'Fwd', 'Rev'];
  const COOLANT_STATES = ['Off', 'Flood', 'Mist'];
  setValue("grblState", `${machine.state}`);
  setValue("grblXposAbs", `${machine.position.x}`);
  setValue("grblYposAbs", `${machine.position.y}`);
  setValue("grblZposAbs", `${machine.position.z}`);
  setValue("grblXpos", `${machine.position.x - machine.workCoordinateOffset.x}`);
  setValue("grblYpos", `${machine.position.y - machine.workCoordinateOffset.y}`);
  setValue("grblZpos", `${machine.position.z - machine.workCoordinateOffset.z}`);
  setValue("grblSpindle", `${SPINDLE_STATES[machine.spindle]}`);
  setValue("grblCoolant", `${COOLANT_STATES[machine.coolant]}`);
  //setValue("grblFR", `${machine.speed.feed}`);
  setValue("grblState", `${machine.state}`);
  if (!refreshjogSet){
    //only write these values first time
    setValue("grblXjog", `${machine.jogDistance.x}`);
    setValue("grblYjog", `${machine.jogDistance.y}`);
    setValue("grblZjog", `${machine.jogDistance.z}`);
    setValue("grblXFR", `${machine.jogFeedrate.x}`);
    setValue("grblYFR", `${machine.jogFeedrate.y}`);
    setValue("grblZFR", `${machine.jogFeedrate.z}`);
    refreshjogSet = true
  }
  let list = ['X-Jog', 'X+Jog','Y-Jog', 'Y+Jog','Z-Jog', 'Z+Jog',
  'grblzeroX', 'grblzeroY', 'grblzeroZ', 'singleLineEntryBtn','grblSendAll']
  for(let w in list){
    setDisable(list[w], !(machine.state=='Idle'));
  }
  //enable/disable individual settings send buttons
  var set = GBRL_SETTINGS['settings']
  var b_text = 'grblSendCmd'
  for (var idx in set) {
    setDisable(b_text.concat('_').concat(set[idx].id), (!machine.connectionStatus || !(machine.state=='Idle')))
  }  
})



const setDisable = (id, state)=>{
  let widget = document.getElementById(id);
  if(!(widget==null)){
    widget.disabled = state;
  }
}
const setValue = (id, value)=>{
  let widget = document.getElementById(id);
  if(!(widget==null)){
    widget.value = value;
  }
}
const initialize = (e)=>
{
  if (!grblConnectedStat){
    //console.log('payload',machine.connectionStatus)
    setDisable('grblPort', false)
    setDisable('grblConnect', false)
    setDisable('grblDisconnect', true)
    setDisable('grblResume', true)
    setDisable('grblHold', true)
    setDisable('singleLineSend', true)
    setDisable('X-Jog', true)
    setDisable('X+Jog', true)
    setDisable('Y-Jog', true)
    setDisable('Y+Jog', true)
    setDisable('Z-Jog', true)
    setDisable('Z+Jog', true)
    setDisable('grblzeroX', true)
    setDisable('grblzeroY', true)
    setDisable('grblzeroZ', true)
    setDisable('saveGRBLsettings', true)
    setDisable('grblSendAll', true)
    setDisable('getGRBLSettings', true)
    setDisable('grblRun', true)
    setDisable('grblHome', true)
    setDisable('grblReset', true)
    //disable individual settings send buttons
    var set = GBRL_SETTINGS['settings']
    var b_text = 'grblSendCmd'
    for (var idx in set) {
      setDisable(b_text.concat('_').concat(set[idx].id), true)
    }
  }
}


const addAlarm = (msg)=>{
  //Add alarm messages 
  let btn = document.getElementById('nav-right-2');
  btn.className = 'btn_flash'
  let str = '\n'
  msg = str.concat(String(alarms.length+1).concat(':  ')).concat(msg)
  alarms.push(msg)
  let widget = document.getElementById('Alarm_area');
  if(!(widget==null)){
    widget.value = (widget.value + msg);
  }
}


const sendToBackend = (signal, payload)=>{
  ipcRenderer.send(signal, JSON.stringify(payload));
}; 

const plcWrite = (t, v)=>
{
  sendToBackend('plcWrite', {tag: t, val: v});
}  

const sendCommand = (cmd, args=[])=>
{
  sendToBackend('cmd', {command: cmd, params: args});
}

/* const backendLog = (data)=>{
  //use this to debug frontend info in the backend console
  sendToBackend('debug-data', data)
} */

const inputEnter = (event)=>{
   // Execute a function when the user releases a key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("singleLineSend").click();
  }
}

const inputChange = (widgetID)=>{
  // Execute a function when the user changes input value for a backend machine value
  if (document.getElementById(widgetID)){
    let x = document.getElementById(widgetID).value
    if (/^\d+\.\d{0,2}$/.test(x) || /^\d*$/.test(x)){
      //validate a number or not
      sendToBackend('updateGRBL_M_Value', {id:widgetID,val:document.getElementById(widgetID).value});
    } else{
      document.getElementById(widgetID).value = '0'
    }
    
  }

}

const loadPage = ()=>{
  //loading initial tabs
  buttonClick({btn: 'nav-left-0'});
  buttonClick({btn: 'nav-right-0'});
  buttonClick({btn: 'comm_PortRefresh'});
  initialize();
}



const buttonClick = (params)=>{
  //all button clicks to be handled here
  switch(true){
    case(params.btn.startsWith("grblSendCmd")):
      var set = GBRL_SETTINGS['settings']
      var val = ''
      var txt = ''
      for (var idx in set) {
        if(set[idx].cmd == params.cmd){
          txt = set[idx].lbl_text
          if (set[idx].widget === 'select'){
            for(var key in set[idx].opt){
              //Convert Select text into integer value to send
              if (set[idx].opt[key] == document.getElementById(set[idx].id.concat('_NEWVAL')).value){
                val = key
              }
            }
          }else{
            val = document.getElementById(set[idx].id.concat('_NEWVAL')).value
          }
        }
      }
      sendCommand('singleSetting',  args=[params.cmd, val, txt]);
      break;
    case(params.btn=="grblSendAll"):
      var set = GBRL_SETTINGS['settings']
      var args = {}
      var val = ''
      var txt = ''
      for (var idx in set) {
          txt = set[idx].lbl_text
          if (set[idx].widget === 'select'){
            for(var key in set[idx].opt){
              //Convert Select text into integer value to send
              if (set[idx].opt[key] == document.getElementById(set[idx].id.concat('_NEWVAL')).value){
                val = key
              }
            }
          }else{
            val = document.getElementById(set[idx].id.concat('_NEWVAL')).value
          }
          if (val){
            args[idx] = {'cmd':set[idx].cmd, 'val':val, 'txt':set[idx].lbl_text}
          }
      }
      sendCommand('multiSetting',  args);
      break;
    case(params.btn=="templateCode"):
      sendToBackend('temp-code', 'grblCode');
    break; 
    case(params.btn.startsWith("nav-left")):
      navBarLeft.btnClick(params.btn);
      break;    
    case(params.btn.startsWith("nav-right")):
      navBarRight.btnClick(params.btn);
      break;
    case(params.btn=="grblConnect"):
      gCommPort = document.getElementById("grblPort").value
      sendToBackend('connect', {controller: 'grbl', port:gCommPort});
      break;
    case(params.btn=="grblDisconnect"):
      gCommPort = ""
      sendToBackend('disconnect', {controller: 'grbl'});
      break;
    case(params.btn=="grblHold"):
      sendCommand('sendGcode', args=['!']);
      break;
    case((params.btn=="X-Jog") || (params.btn=="X+Jog") || (params.btn=="Y-Jog") || (params.btn=="Y+Jog" ||
    (params.btn=="Z-Jog" || (params.btn=="Z+Jog")))):
      var distance = 0
      var fdrt = 0
      if (params.btn.startsWith('X')){
        distance = document.getElementById('grblXjog').value
        fdrt = document.getElementById('grblXFR').value
      }
      if (params.btn.startsWith('Y')){
        distance = document.getElementById('grblYjog').value
        fdrt = document.getElementById('grblYFR').value
      }
      if (params.btn.startsWith('Z')){
        distance = document.getElementById('grblZjog').value
        fdrt = document.getElementById('grblZFR').value
      }
      sendCommand('jog', args={btn:params.btn,feed:fdrt,dis:distance});
      break;
    case(params.btn.startsWith('grblzero')):
      let axis = params.btn.replace(RegExp(`grblzero`, 'g'), '')
      sendCommand('zeroAxis', args=[axis]);
      break;
    case(params.btn=="grblResume"):
      sendCommand('sendGcode', args=['~']);
      break;
    case(params.btn=="grblReset"):
      // Button does not exist
      sendToBackend('grbl-reset', {});
      break;      
    case(params.btn=="grblHome"):
      sendToBackend('grbl-home', {});
      break;
    case(params.btn=="grblCoolantOff"):
      sendCommand('sendGcode', args=['M9']);
      break;
    case(params.btn=="grblCoolantOn"):
      sendCommand('sendGcode', args=['M8']);
      break;
    case(params.btn=="grblSpindleOff"):
      sendCommand('sendGcode', args=['M5']);
      break;
    case(params.btn=="grblSpindleFwd"):
      sendCommand('sendGcode', args=['M3']);
      break;
    case(params.btn=="grblSpindleRev"):
      sendCommand('sendGcode', args=['M4']);
      break;
    case(params.btn=="singleLineSend"):
      let line = document.getElementById("singleLineEntry").value
      sendCommand('sendGcode-line', args=[line]);
      break;
    case(params.btn=="grblRun"):
      let gcode = document.getElementById("gCodeArea").value
      sendCommand('sendGcode-program', args=[gcode]);
      break;
    case(params.btn=="plcConnect"):
      pCommPort = document.getElementById("plcPort").value
      sendToBackend('connect', {controller: 'plc', port:pCommPort});
      break;
    case(params.btn=="plcDisconnect"):
      pCommPort = ""
      sendToBackend('disconnect', {controller: 'plc'});
      break;
    case(params.btn=="plcSpindleStop"):
      plcWrite('SPDL_STOP_CMD', 1);
      break;
    case(params.btn=="plcSpindleRun"):
      plcWrite('SPDL_RUN_CMD', 1);
      break;
    case(params.btn=="plcSpindleFwd"):
      plcWrite('SPDL_FWD_CMD', 1);
      break;
    case(params.btn=="plcSpindleRev"):
      plcWrite('SPDL_REV_CMD', 1);
      break;
    case(params.btn=="plcBrakeOff"):
      plcWrite('BRK_OFF_CMD', 1);
      break;
    case(params.btn=="plcBrakeOn"):
      plcWrite('BRK_ON_CMD', 1);
      break;
    case(params.btn=="plcLimitOverride"):
      plcWrite('LS_OVR_RDE', 1);
      break;
    case(params.btn=="plcLockoutRst"):
      plcWrite('LCK_OUT_RST', 1);
      break;
    case(params.btn=="plcToolIn"):
      plcWrite('TC_IN_CMD', 1);
      break;
    case(params.btn=="plcToolOut"):
      plcWrite('TC_OUT_CMD', 1);
      break;
    case(params.btn=="plcAuto"):
      // Button does not exist
      addAlarm("Not implemented in the PLC yet");
      break;
    case(params.btn=="plcManual"):
      // Button does not exist
      addAlarm("Not implemented in the PLC yet");
      break;
    case((params.btn=="comm_PortRefresh")):
      sendToBackend('comm-refresh', {});
      break;
    case(params.btn=="openGcodeFile"):
      sendToBackend('openGcodeFile', {});
      break;
    case(params.btn=="saveGcodeFile"):
      let ta = document.getElementById('gCodeArea')
      if(ta != null){
        gCodeText = ta.value;
      }
      sendToBackend('saveGcodeFile', gCodeText);
      break;
    case(params.btn=="getGRBLSettings"):
      sendToBackend('getGRBLSettings', {});
      break;
    case(params.btn=="openGRBLSettingsFile"):
      sendToBackend('openGRBLSettingsFile', {});
      break;
    case(params.btn=="saveGRBLsettings"):
      let sett = GBRL_SETTINGS['settings']
      arr = {}
      for (var idx in sett) {
        if (idx != 'Header'){
          if (document.getElementById(sett[idx].id.concat('_VAL')).value.length){
            arr[sett[idx].lbl_text] = document.getElementById(sett[idx].id.concat('_VAL')).value
          }else{
            arr[sett[idx].lbl_text] = sett[idx].default
          }
        }
      }
      sendToBackend('saveGRBLsettings', arr);
      break;
    case(params.btn=="exit"):
      sendToBackend('app-exit', {});
      break;
    case(params.btn=="popup"):
      sendToBackend('confirm', {});
      break;
      
    default:
      addAlarm(`Unhandled button click: ${params.btn}`);
  }
}









